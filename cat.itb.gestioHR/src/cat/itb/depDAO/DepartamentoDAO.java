package cat.itb.depDAO;

public interface DepartamentoDAO
{
    boolean InsertarDep(Departamento dep);

    boolean EliminarDep(int deptno);

    boolean ModificarDep(Departamento dep);

    Departamento ConsultarDep(int deptno);
}