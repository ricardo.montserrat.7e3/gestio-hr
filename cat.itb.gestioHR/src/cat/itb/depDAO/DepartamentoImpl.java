package cat.itb.depDAO;

import cat.itb.DAOFactory.BaseDeDades;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DepartamentoImpl implements DepartamentoDAO
{
    Connection conexion;

    public DepartamentoImpl(Connection conexion) { this.conexion = conexion; }

    public boolean InsertarDep(Departamento dep)
    {
        boolean valor = false;
        try(PreparedStatement sentencia = conexion.prepareStatement("INSERT INTO departamentos VALUES(?, ?, ?)"))
        {
            sentencia.setInt(1, dep.getDeptno());
            sentencia.setString(2, dep.getDnombre());
            sentencia.setString(3, dep.getLoc());
            valor = sentencia.executeUpdate() > 0;
            if(valor) System.out.printf("Departamento %d insertado%n", dep.getDeptno());
        }
        catch (SQLException e) { BaseDeDades.MensajeExcepcion(e); }
        return valor;
    }

    @Override
    public boolean EliminarDep(int deptno) {
        boolean valor = false;
        try(PreparedStatement sentencia = conexion.prepareStatement("DELETE FROM departamentos WHERE dept_no = ? "))
        {
            sentencia.setInt(1, deptno);
            valor = sentencia.executeUpdate() > 0;
            if(valor) System.out.printf("Departamento %d eliminado%n", deptno);
        }
        catch (SQLException e) { BaseDeDades.MensajeExcepcion(e); }
        return valor;
    }

    @Override
    public boolean ModificarDep(Departamento dep) {
        boolean valor = false;
        try(PreparedStatement sentencia = conexion.prepareStatement("UPDATE departamentos SET dnombre= ?, loc = ? WHERE dept_no = ? "))
        {
            sentencia.setInt(3, dep.getDeptno());
            sentencia.setString(1, dep.getDnombre());
            sentencia.setString(2, dep.getLoc());
            valor = sentencia.executeUpdate() > 0;
            if(valor) System.out.printf("Departamento %d modificado%n", dep.getDeptno());
        }
        catch (SQLException e) { BaseDeDades.MensajeExcepcion(e); }
        return valor;
    }

    @Override
    public Departamento ConsultarDep(int deptno) {
        Departamento dep = new Departamento();
        try(ResultSet rs = conexion.prepareStatement("SELECT dept_no, dnombre, loc FROM departamentos WHERE dept_no = "+ deptno).executeQuery())
        {
            if (rs.next())
            {
                dep.setDeptno(rs.getInt("dept_no"));
                dep.setDnombre(rs.getString("dnombre"));
                dep.setLoc(rs.getString("loc"));
            }
            else System.out.printf("Departamento: %d No existe%n", deptno);
        }
        catch (SQLException e) { BaseDeDades.MensajeExcepcion(e); }
        return dep;
    }
}