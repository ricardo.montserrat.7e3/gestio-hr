package cat.itb.DAOFactory;

import cat.itb.depDAO.DepartamentoDAO;
import cat.itb.depDAO.DepartamentoImpl;
import cat.itb.empDAO.EmpleadoDAO;
import cat.itb.empDAO.EmpleadoImpl;

import java.sql.Connection;

public class SqlDbDAOFactory extends DAOFactory {
    private static Connection conexion = null;
    private static String URL = "";
    private static String DB = "";
    private static String USUARIO = "user1";
    private static String CLAVE = "password1";

    public SqlDbDAOFactory()
    {
        URL = "jdbc:mysql://localhost:3306/";
        DB = "HR";

        conexion = BaseDeDades.connectar(URL, DB, USUARIO, CLAVE);
    }

    @Override
    public DepartamentoDAO getDepartamentoDAO() { return new DepartamentoImpl(conexion); }

    @Override
    public EmpleadoDAO getEmpleadoDAO() { return new EmpleadoImpl(conexion); }
}