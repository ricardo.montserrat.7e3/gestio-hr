package cat.itb.DAOFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class BaseDeDades
{
    public final static String URL = "jdbc:mysql://localhost:3306/";
    public final static String BD = "HR";
    public final static String USER = "user1";
    public final static String PASSWORD = "password1";

    public static Connection connectar() { return connectar(URL, BD, USER, PASSWORD); }

    public static Connection connectar(String url, String bd, String user, String password)
    {
        Connection conexio = null;
        try
        {
            conexio = DriverManager.getConnection(url + bd, user, password);
            System.out.println("¡Connexió Exitosa!");
        }
        catch (SQLException e) { MensajeExcepcion(e); }
        return conexio;
    }
    public static void MensajeExcepcion(SQLException e)
    {
        System.out.printf("HA OCURRIDO UNA EXCEPCIÓN:%n");
        System.out.printf("Mensaje : %s %n", e.getMessage());
        System.out.printf("SQL estado: %s %n", e.getSQLState());
        System.out.printf("Cód error : %s %n", e.getErrorCode());
    }
}