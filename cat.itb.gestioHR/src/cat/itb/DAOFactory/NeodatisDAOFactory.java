package cat.itb.DAOFactory;

import cat.itb.depDAO.DepartamentoDAO;
import cat.itb.empDAO.EmpleadoDAO;

public class NeodatisDAOFactory extends DAOFactory
{
    @Override
    public DepartamentoDAO getDepartamentoDAO() { return null; }

    @Override
    public EmpleadoDAO getEmpleadoDAO() { return null; }
}
