package cat.itb.empDAO;

import cat.itb.DAOFactory.BaseDeDades;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class EmpleadoImpl implements EmpleadoDAO
{
    Connection conexion;

    public EmpleadoImpl(Connection conexion) { this.conexion = conexion; }

    @Override
    public boolean InsertarEmp(Empleado empleado)
    {
        boolean valor;
        try(PreparedStatement sentencia = conexion.prepareStatement("INSERT INTO empleados VALUES(?, ?, ?, ?, ? ,? ,?, ?)"))
        {
            sentencia.setInt(1, empleado.getEmpNO());
            sentencia.setString(2, empleado.getApellido());
            sentencia.setString(3, empleado.getOficio());
            sentencia.setInt(4, empleado.getDir());
            sentencia.setDate(5, empleado.getFechaAlt());
            sentencia.setFloat(6, empleado.getSalario());
            sentencia.setFloat(7, empleado.getComision());
            sentencia.setInt(8, empleado.getDeptNo());
            valor = sentencia.executeUpdate() > 0;
            if(valor) System.out.printf("Empleado %d insertado%n", empleado.getEmpNO());
        }
        catch (SQLException e) { BaseDeDades.MensajeExcepcion(e); }
        return false;
    }

    @Override
    public boolean EliminarEmp(int empleadoNo)
    {
        boolean valor = false;
        try(PreparedStatement sentencia = conexion.prepareStatement("DELETE FROM empleados WHERE emp_no = ? "))
        {
            sentencia.setInt(1, empleadoNo);
            valor = sentencia.executeUpdate() > 0;
            if(valor) System.out.printf("Empleado %d eliminado%n", empleadoNo);
        }
        catch (SQLException e) { BaseDeDades.MensajeExcepcion(e); }
        return valor;
    }

    @Override
    public boolean ModificarEmp(Empleado empleado)
    {
        boolean valor = false;
        try(PreparedStatement sentencia = conexion.prepareStatement("UPDATE empleados SET apellido= ?, oficio= ?, dir= ?, fecha_alt= ?, salario= ?, comision= ?, dept_no= ? WHERE emp_no = ? "))
        {
            sentencia.setInt(8, empleado.getEmpNO());
            sentencia.setString(1, empleado.getApellido());
            sentencia.setString(2, empleado.getOficio());
            sentencia.setInt(3, empleado.getDir());
            sentencia.setDate(4, empleado.getFechaAlt());
            sentencia.setFloat(5, empleado.getSalario());
            sentencia.setFloat(6, empleado.getComision());
            sentencia.setInt(7, empleado.getDeptNo());
            valor = sentencia.executeUpdate() > 0;
            if(valor) System.out.printf("Empleado %d modificado%n", empleado.getEmpNO());
        }
        catch (SQLException e) { BaseDeDades.MensajeExcepcion(e); }
        return valor;
    }

    @Override
    public Empleado ConsultarEmp(int empleadoNo)
    {
        Empleado emp = new Empleado();
        try(ResultSet rs = conexion.prepareStatement("SELECT * FROM empleados WHERE emp_no = "+ empleadoNo).executeQuery())
        {
            if (rs.next())
            {
                emp.setEmpNO(rs.getInt("emp_no"));
                emp.setApellido(rs.getString("apellido"));
                emp.setOficio(rs.getString("oficio"));
                emp.setDir(rs.getInt("dir"));
                emp.setFechaAlt(rs.getDate("fecha_alt"));
                emp.setSalario(rs.getFloat("salario"));
                emp.setComision(rs.getFloat("comision"));
                emp.setDeptNo(rs.getInt("dept_no"));
            }
            else System.out.printf("Empleado: %d No existe%n", emp.getEmpNO());
        }
        catch (SQLException e) { BaseDeDades.MensajeExcepcion(e); }
        return emp;
    }
}
