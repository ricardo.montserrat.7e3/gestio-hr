package cat.itb.empDAO;

import cat.itb.depDAO.Departamento;

public interface EmpleadoDAO
{
    boolean InsertarEmp(Empleado empleado);

    boolean EliminarEmp(int empleado);

    boolean ModificarEmp(Empleado empleado);

    Empleado ConsultarEmp(int empleado);
}
