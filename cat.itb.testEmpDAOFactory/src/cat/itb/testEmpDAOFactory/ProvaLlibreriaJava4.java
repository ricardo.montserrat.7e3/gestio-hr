package cat.itb.testEmpDAOFactory;

import cat.itb.DAOFactory.DAOFactory;
import cat.itb.empDAO.Empleado;
import cat.itb.empDAO.EmpleadoDAO;

import java.util.Scanner;

public class ProvaLlibreriaJava4
{
    public static void main(String[] args)
    {
        DAOFactory bd = DAOFactory.getDAOFactory(DAOFactory.MYSQL);
        EmpleadoDAO empDAO = bd.getEmpleadoDAO();

        //CONSULTAR
        Empleado emp2 = empDAO.ConsultarEmp(7369);
        System.out.printf("Codi: %d, Nom: %s%n", emp2.getEmpNO(), emp2.getApellido());

//MODIFICAR
        emp2.setApellido("Ramos");
        emp2.setDir(6969);
        empDAO.ModificarEmp(emp2);
        System.out.printf("Codi: %d, Nom: %s%n", emp2.getEmpNO(), emp2.getApellido());

//ELIMINAR
        empDAO.EliminarEmp(7369);

        //Visualitza les dades del empartament que introdueixes per teclat
        Scanner sc = new Scanner(System.in);


        Empleado emp;
        int entero = 1;
        while (entero > 0)
        {
            System.out.println("Empleado: ");
            emp = empDAO.ConsultarEmp(entero = sc.nextInt());
            System.out.printf("Codi: %d, Nom: %s%n", emp.getEmpNO(), emp.getApellido());
        }
    }
}
