package cat.itb.testDepDAOFactory;

import cat.itb.DAOFactory.DAOFactory;
import cat.itb.depDAO.Departamento;
import cat.itb.depDAO.DepartamentoDAO;

import java.util.Scanner;

public class ProvaLlibreriaJava3 {
    public static void main(String[] args)
    {
        DAOFactory bd = DAOFactory.getDAOFactory(DAOFactory.MYSQL);
        DepartamentoDAO depDAO = bd.getDepartamentoDAO();

//crear departamento
        Departamento dep = new Departamento(18, "COMPTABILITAT", "REUS");

//INSERTAR
        depDAO.InsertarDep(dep);

//CONSULTAR
        Departamento dep2 = depDAO.ConsultarDep(18);
        System.out.printf("Dep: %d, Nom: %s, Loc: %s %n", dep2.getDeptno(), dep2.getDnombre(), dep2.getLoc());

//MODIFICAR
        dep2.setDnombre("nounom");
        dep2.setLoc("novaloc");
        depDAO.ModificarDep(dep2);
        System.out.printf("Dep: %d, Nom: %s, Loc: %s %n", dep2.getDeptno(), dep2.getDnombre(), dep2.getLoc());

//ELIMINAR
        depDAO.EliminarDep(18);

//Visualitza les dades del departament que introdueixes per teclat
        Scanner sc = new Scanner(System.in);
        int entero = 1;
        while (entero > 0)
        {
            System.out.println("Departamento: ");
            dep = depDAO.ConsultarDep(entero = sc.nextInt());
            System.out.printf("Dep: %d, Nombre: %s, Loc: %s %n", dep.getDeptno(), dep.getDnombre(), dep.getLoc());
        }
    }
}